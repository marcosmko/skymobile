//
//  Movie.swift
//  Infrastructure
//
//  Created by Marcos Kobuchi on 13/08/18.
//  Copyright © 2018 Marcos Kobuchi. All rights reserved.
//

import Foundation

public class Movie: Decodable {

    public var id: String!
    public var title: String!
    public var overview: String!
    public var duration: String!
    public var release_year: String!
    public var cover_url: String!

    public init() {}
}
