//
//  SkyMobileUITests.swift
//  SkyMobileUITests
//
//  Created by Marcos Kobuchi on 13/08/18.
//  Copyright © 2018 Marcos Kobuchi. All rights reserved.
//

import XCTest

class MoviewUITests: XCTestCase {
    var app: XCUIApplication!
        
    override func setUp() {
        super.setUp()
        
        continueAfterFailure = false
        self.app = XCUIApplication()
        XCUIApplication().launch()
    }
    
    func testObserveMovieDoutorEstranho() {
        self.app.launch()
        
        let cell = app.collectionViews.cells.staticTexts["Doutor Estranho"]
        let exists = NSPredicate(format: "exists == 1")
        
        expectation(for: exists, evaluatedWith: cell, handler: nil)
        waitForExpectations(timeout: 5, handler: nil)
    }
    
}
