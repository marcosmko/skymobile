//
//  ViewController.swift
//  SkyMobile
//
//  Created by Marcos Kobuchi on 13/08/18.
//  Copyright © 2018 Marcos Kobuchi. All rights reserved.
//

import UIKit
import Persistence

class MoviesViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    private lazy var manager: MoviesManager = MoviesManager(delegate: self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.manager.getMovies()
    }

}

extension MoviesViewController: MoviesManagerDelegate {

    func success() {
        self.collectionView.reloadData()
    }
    
    func failure(error: String) {
        print("falhou")
    }

}

extension MoviesViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.manager.numberOfMovies()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "movieCell", for: indexPath) as? MovieCollectionViewCell else { fatalError("cell not registered!") }
        
        let movieViewModel: MovieViewModel = self.manager.movie(at: indexPath.row)
        cell.id = movieViewModel.id
        cell.titleLabel.text = movieViewModel.title
        
        cell.activityIndicatorView.startAnimating()
        self.manager.image(poster: movieViewModel.cover_url) { (image) in
            if cell.id == movieViewModel.id {
                cell.imageView?.image = image
                cell.activityIndicatorView.stopAnimating()
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let viewSize: CGSize = collectionView.frame.size
        let width: CGFloat = viewSize.width / 2
        let height: CGFloat = width * 3.5 / 2
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
        case UICollectionElementKindSectionHeader:
            return collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "header", for: indexPath)
        default:
            assert(false, "Unexpected element kind")
        }
    }
    
}
