//
//  MoviesManager.swift
//  SkyMobile
//
//  Created by Marcos Kobuchi on 13/08/18.
//  Copyright © 2018 Marcos Kobuchi. All rights reserved.
//

import Foundation
import Infrastructure
import Services

protocol MoviesManagerDelegate: class {
    func success()
    func failure(error: String)
}

class MoviesManager {
    
    weak var delegate: MoviesManagerDelegate?
    private let movieServices: MovieServicesProtocol.Type
    private let dataServices: DataServicesProtocol.Type
    
    private var movies: [Movie] = []
    
    init(delegate: MoviesManagerDelegate,
         movieServices: MovieServicesProtocol.Type = MovieServices.self,
         dataServices: DataServicesProtocol.Type = DataServices.self) {
        self.delegate = delegate
        self.movieServices = movieServices
        self.dataServices = dataServices
    }
    
    func getMovies() {
        movieServices.movies { (movies, error) in
            if let error = error {
                self.movies = []
                self.delegate?.failure(error: error.localizedDescription)
            } else {
                self.movies = movies
                self.delegate?.success()
            }
        }
    }
    
    func numberOfMovies() -> Int {
        return self.movies.count
    }
    
    func movie(at index: Int) -> MovieViewModel {
        let movie: Movie = self.movies[index]
        return MovieViewModel(id: movie.id, title: movie.title, overview: movie.overview, cover_url: movie.cover_url)
    }
    
    func clear() {
        self.movies = []
    }
    
    func image(poster: String, completion: @escaping ((UIImage) -> Void)) {
        dataServices.shared.data(for: poster) { (data) in
            if let image = UIImage(data: data) {
                completion(image)
            } else {
                // should not happen
                completion(UIImage())
            }
        }
    }

}
