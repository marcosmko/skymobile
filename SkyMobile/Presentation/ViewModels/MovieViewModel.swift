//
//  MovieViewModel.swift
//  SkyMobile
//
//  Created by Marcos Kobuchi on 13/08/18.
//  Copyright © 2018 Marcos Kobuchi. All rights reserved.
//

import Foundation

struct MovieViewModel {
    let id: String
    let title: String
    let overview: String
    var cover_url: String
}
