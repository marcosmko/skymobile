//
//  Server+Movies.swift
//  Persistence
//
//  Created by Marcos Kobuchi on 13/08/18.
//  Copyright © 2018 Marcos Kobuchi. All rights reserved.
//

import Foundation
import Infrastructure

public protocol MoviesProvider {
    static func list() throws -> [Movie]
}

public extension Server {

    public struct Movies: MoviesProvider {
        
        public static func list() throws -> [Movie] {
            return try Server.request(method: .get, endpoint: "Movies")
        }
        
    }

}
