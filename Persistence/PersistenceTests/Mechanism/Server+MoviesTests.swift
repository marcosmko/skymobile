//
//  Server+MoviesTests.swift
//  Persistence
//
//  Created by Marcos Kobuchi on 13/08/18.
//  Copyright © 2018 Marcos Kobuchi. All rights reserved.
//

import XCTest
import Infrastructure
@testable import Persistence

class ServerMoviesTests: XCTestCase {
    
    func testRequestMovies() {
        do {
            let movies: [Movie] = try Server.Movies.list()
            XCTAssertEqual(movies.count, 2)
            XCTAssertEqual(movies[0].id, "090f0d8fs9d0dfdf")
            XCTAssertEqual(movies[1].id, "43294394329h32")
        } catch {
            XCTFail("unexpected: \(error.localizedDescription)")
        }
    }
    
}
