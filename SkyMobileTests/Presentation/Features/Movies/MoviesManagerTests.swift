//
//  SkyMobileTests.swift
//  SkyMobileTests
//
//  Created by Marcos Kobuchi on 13/08/18.
//  Copyright © 2018 Marcos Kobuchi. All rights reserved.
//

import XCTest
import Infrastructure
import Services
@testable import SkyMobile

// swiftlint:disable nesting
class MoviesManagerTests: XCTestCase {
    
    // mock all services
    private class MovieServicesMock: MovieServicesProtocol {
        static func movies(_ completion: (([Movie], Error?) -> Void)?) {
            let movie1: Movie = Movie()
            let movie2: Movie = Movie()
            let movie3: Movie = Movie()
            
            movie1.id = "id_1"
            movie1.title = "title_1"
            movie1.overview = "overview_1"
            movie1.cover_url = "cover_1"
            
            movie2.id = "id_2"
            movie3.id = "id_3"
            
            completion?([movie1, movie2, movie3], nil)
        }
    }
    
    private class DataServicesMock: DataServicesProtocol {
        static var shared: DataServicesProtocol = DataServicesMock()
        func data(for path: String, _ completion: ((Data) -> Void)?) {
            if path == "path 1" {
                completion?(Data())
            } else {
                XCTFail("path not mapped")
            }
        }
    }
    
    func testMoviesManagerSearchSuccess() {
        let expectation: XCTestExpectation = XCTestExpectation(description: "success")
        class Delegate: MoviesManagerDelegate {
            let expectation: XCTestExpectation
            init(expectation: XCTestExpectation) {
                self.expectation = expectation
            }
            
            func failure(error: String) { }
            func success() { self.expectation.fulfill() }
        }
        
        let delegate: Delegate = Delegate(expectation: expectation)
        let manager: MoviesManager = MoviesManager(delegate: delegate,
                                                   movieServices: MovieServicesMock.self,
                                                   dataServices: DataServicesMock.self)
        
        manager.getMovies()
        wait(for: [expectation], timeout: 0.5)

        XCTAssertEqual(manager.numberOfMovies(), 3)
        let movie: MovieViewModel = manager.movie(at: 0)
        XCTAssertEqual(movie.id, "id_1")

        manager.clear()
        XCTAssertEqual(manager.numberOfMovies(), 0)
    }
    
    func testImageForPoster() {
        class Delegate: MoviesManagerDelegate {
            func failure(error: String) { }
            func success() { }
        }

        let delegate: Delegate = Delegate()
        let manager: MoviesManager = MoviesManager(delegate: delegate,
                                                   movieServices: MovieServicesMock.self,
                                                   dataServices: DataServicesMock.self)

        let expectation: XCTestExpectation = XCTestExpectation(description: "image")
        manager.image(poster: "path 1") { (_) in
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 0.5)
    }

    
}
