//
//  MovieServicesTests.swift
//  Services
//
//  Created by Marcos Kobuchi on 13/08/18.
//  Copyright © 2018 Marcos Kobuchi. All rights reserved.
//

import XCTest
import Infrastructure
import Persistence
@testable import Services

class MovieServicesTests: XCTestCase {
    private struct Provider: MoviesProvider {
        static func list() throws -> [Movie] {
            let movie: Movie = Movie()
            movie.id = "id"
            movie.title = "title"
            movie.overview = "overview"
            movie.cover_url = "cover"
            movie.duration = "duration"
            movie.release_year = "release"
            return [movie]
        }
    }
    
    var defaultProvider: MoviesProvider.Type!
    override func setUp() {
        super.setUp()
        self.defaultProvider = MovieServices.provider
        MovieServices.provider = Provider.self
    }
    
    override func tearDown() {
        super.tearDown()
        MovieServices.provider = defaultProvider
    }
    
    func testRequestMoviesSuccess() {
        let expectation: XCTestExpectation = XCTestExpectation(description: "expectation")
        
        MovieServices.movies { (movies, error) in
            XCTAssertNil(error)
            XCTAssertEqual(movies.count, 1)
            XCTAssertEqual(movies[0].id, "id")
            XCTAssertEqual(movies[0].title, "title")
            XCTAssertEqual(movies[0].overview, "overview")
            XCTAssertEqual(movies[0].cover_url, "cover")
            XCTAssertEqual(movies[0].duration, "duration")
            XCTAssertEqual(movies[0].release_year, "release")
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 1)
    }
    
}
