//
//  MovieServices.swift
//  Services
//
//  Created by Marcos Kobuchi on 13/08/18.
//  Copyright © 2018 Marcos Kobuchi. All rights reserved.
//

import Foundation
import Infrastructure
import Persistence

public protocol MovieServicesProtocol: class {
    static func movies(_ completion: ((_ movies: [Movie], _ error: Error?) -> Void)?)
}

public class MovieServices: MovieServicesProtocol {
    
    private init() {}
    static var provider: MoviesProvider.Type = Server.Movies.self

    public static func movies(_ completion: ((_ movies: [Movie], _ error: Error?) -> Void)?) {
        let blockForExecutionInBackground: BlockOperation = BlockOperation(block: {
            var raisedError: Error?
            var movies: [Movie] = []
            
            do {
                movies = try self.provider.list()
            } catch let error {
                raisedError = error
            }
            
            if let completion = completion {
                let blockForExecutionInMain: BlockOperation = BlockOperation(block: { completion(movies, raisedError) })
                QueueManager.sharedInstance.executeBlock(blockForExecutionInMain, queueType: QueueManager.QueueType.main)
            }
        })
        
        QueueManager.sharedInstance.executeBlock(blockForExecutionInBackground, queueType: QueueManager.QueueType.serial)
    }

}
